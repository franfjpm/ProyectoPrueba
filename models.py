
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Minion(models.Model):
	nombre_texto = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	def __unicode__(self):
		return self.nombre_texto
