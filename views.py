from django.shortcuts import get_object_or_404, render
from .models import Minion

import salt.client
import salt.config
import salt.loader
# Create your views here.
def index(request):
	latest_question_list = Minion.objects.order_by('-pub_date')[:5]
	local = salt.client.LocalClient()
    	nombre=local.cmd('*', 'cmd.run', ['whoami'])
    	context = {'latest_question_list': latest_question_list, 'nombre':nombre}
    	return render(request, 'indice.html', context)
def detalle(request, minion_id):
	minion = get_object_or_404(Minion, pk=minion_id)
	__opts__ = salt.config.minion_config('/etc/salt/minion')
	__grains__ = salt.loader.grains(__opts__)
	__opts__['grains'] = __grains__
	__utils__ = salt.loader.utils(__opts__)
	__salt__ = salt.loader.minion_mods(__opts__, utils=__utils__)
	x=__salt__['service.status']('nginx')
	context = {'minion': minion, 'x':x}
    	return render(request, 'detalle.html', context)
